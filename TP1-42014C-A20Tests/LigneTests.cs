using NUnit.Framework;
using System;
using System.Drawing;
using Tp1_42014C_A20;

namespace TP1_42014C_A20Tests
{
    public class LigneTests
    {
        [SetUp]
        public void Setup()
        {
        }

        [Test]
        public void ConstructeurTestAvecParametreValide()
        {
            Point pointDepart = new Point(1, 3);
            Point pointFin = new Point(2, 5);
            Ligne uneLigne = new Ligne(Color.Black, pointDepart, pointFin, 1);
            Assert.AreEqual(Color.Black, uneLigne.Couleur);
            Assert.AreEqual(1, uneLigne.LargeurTrait);
            Assert.AreEqual(pointDepart, uneLigne.PointDepart);
            Assert.AreEqual(pointFin, uneLigne.PointFin);
        }

        [Test]
        public void ConstructeurTestAvecPremierParamettreInvalide()
        {
            Point pointDepart = new Point(1, 3);
            Point pointFin = new Point(2, 5);
            Ligne uneligne = null;

            try
            {
                uneligne = new Ligne(Color.Empty, pointDepart, pointFin, 1);
                Assert.Fail();
            }
            catch (ArgumentNullException e)
            {
                Assert.IsNull(uneligne);
                Assert.AreEqual("La couleur est nulle.", e.Message);
            }
        }

        [Test]
        public void ConstructeurTestAvecDeuxiemeParamettreInvalide()
        {
            Point pointFin = new Point(2, 5);
            Ligne uneligne = null;

            try
            {
                uneligne = new Ligne(Color.Black, Point.Empty, pointFin, 1);
                Assert.Fail();
            }
            catch (ArgumentNullException e)
            {
                Assert.IsNull(uneligne);
                Assert.AreEqual("Le point de d�part est nul.", e.Message);
            }

            try
            {
                uneligne = new Ligne(Color.Black, Point.Empty, Point.Empty, 0);
                Assert.Fail();
            }
            catch (ArgumentNullException e)
            {
                Assert.IsNull(uneligne);
                Assert.AreEqual("Le point de d�part est nul.", e.Message);
            }

            try
            {
                uneligne = new Ligne(Color.Black, Point.Empty, pointFin, 0);
                Assert.Fail();
            }
            catch (ArgumentNullException e)
            {
                Assert.IsNull(uneligne);
                Assert.AreEqual("Le point de d�part est nul.", e.Message);
            }
        }

        [Test]
        public void ConstructeurTestAvecTroisiemeParamettreInvalide()
        {
            Point pointDepart = new Point(1, 3);
            Ligne uneligne = null;

            try
            {
                uneligne = new Ligne(Color.Black, pointDepart, Point.Empty, 1);
                Assert.Fail();
            }
            catch (ArgumentNullException e)
            {
                Assert.IsNull(uneligne);
                Assert.AreEqual("Le point de fin est nul.", e.Message);
            }
        }

        [Test]
        public void TesterAccesseurCouleurEnEcriture()
        {
            Point pointDepart = new Point(1, 3);
            Point pointFin = new Point(2, 5);
            Ligne uneLigne = new Ligne(Color.Black, pointDepart, pointFin, 1)
            {
                Couleur = Color.Aqua
            };
            Assert.AreEqual(Color.Aqua, uneLigne.Couleur);
            Assert.AreEqual(new Point(1, 3), uneLigne.PointDepart);
            Assert.AreEqual(new Point(2, 5), uneLigne.PointFin);
            Assert.AreEqual(1, uneLigne.LargeurTrait);
            try
            {
                uneLigne.Couleur = Color.Empty;
                Assert.Fail();
            }
            catch (ArgumentNullException e)
            {
                Assert.AreEqual(Color.Aqua, uneLigne.Couleur);
                Assert.AreEqual(new Point(1, 3), uneLigne.PointDepart);
                Assert.AreEqual(new Point(2, 5), uneLigne.PointFin);
                Assert.AreEqual(1, uneLigne.LargeurTrait);
                Assert.AreEqual("La couleur est nulle.", e.Message);
            }
        }
    }
}