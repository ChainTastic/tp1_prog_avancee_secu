using NUnit.Framework;
using System;
using System.Drawing;
using Tp1_42014C_A20;
using Rectangle = Tp1_42014C_A20.Rectangle;

namespace TP1_42014C_A20Tests
{
    public class FormeTests
    {
        [SetUp]
        public void Setup()
        {
        }

        [Test]
        public void ConstructeurTestAvecParametreValide()
        {
            Point pointDepart = new Point(1, 3);
            Point pointFin = new Point(2, 5);

            Forme uneForme = new Rectangle(Color.Blue, Color.Blue, pointDepart, pointFin, 1);

            Assert.AreEqual(Color.Blue, uneForme.CouleurBordure);
            Assert.AreEqual(Color.Blue, uneForme.CouleurRemplissage);
            Assert.AreEqual(pointDepart, uneForme.PointDepart);
            Assert.AreEqual(pointFin, uneForme.PointFin);
            Assert.AreEqual(1, uneForme.LargeurTrait);
        }

        [Test]
        public void ConstructeurTestAvecCouleurBordureInvalide()
        {
            Point pointDepart = new Point(1, 3);
            Point pointFin = new Point(2, 5);
            Ellipse uneEllipse = null;

            try
            {
                uneEllipse = new Ellipse(Color.Empty, Color.Blue, pointDepart, pointFin, 1);
                Assert.Fail();
            }
            catch (ArgumentNullException e)
            {
                Assert.IsNull(uneEllipse);
                Assert.AreEqual("La couleur de bordure est nulle.", e.Message);
            }

            try
            {
                uneEllipse = new Ellipse(Color.Empty, Color.Empty, pointDepart, pointFin, 1);
                Assert.Fail();
            }
            catch (ArgumentNullException ex)
            {
                Assert.IsNull(uneEllipse);
                Assert.AreEqual("La couleur de bordure est nulle.", ex.Message);
            }
        }

        [Test]
        public void ConstructeurTestAvecCouleurRemplissageInvalide()
        {
            Point pointDepart = new Point(1, 3);
            Point pointFin = new Point(2, 5);
            Ellipse uneEllipse = null;

            try
            {
                uneEllipse = new Ellipse(Color.Blue, Color.Empty, pointDepart, pointFin, 1);
                Assert.Fail();
            }
            catch (ArgumentNullException e)
            {
                Assert.IsNull(uneEllipse);
                Assert.AreEqual("La couleur de remplissage est nulle.", e.Message);
            }
        }

        [Test]
        public void TesterAccesseurCouleurBordureEnEcriture()
        {
            Point pointDepart = new Point(1, 3);
            Point pointFin = new Point(2, 5);
            Ellipse uneEllipse = new Ellipse(Color.Black, Color.Blue, pointDepart, pointFin, 1)
            {
                CouleurBordure = Color.Aqua
            };
            Assert.AreEqual(Color.Aqua, uneEllipse.CouleurBordure);
            Assert.AreEqual(Color.Blue, uneEllipse.CouleurRemplissage);
            Assert.AreEqual(new Point(1, 3), uneEllipse.PointDepart);
            Assert.AreEqual(new Point(2, 5), uneEllipse.PointFin);
            Assert.AreEqual(1, uneEllipse.LargeurTrait);
            try
            {
                uneEllipse.CouleurBordure = Color.Empty;
                Assert.Fail();
            }
            catch (ArgumentNullException e)
            {
                Assert.AreEqual(Color.Aqua, uneEllipse.CouleurBordure);
                Assert.AreEqual(Color.Blue, uneEllipse.CouleurRemplissage);
                Assert.AreEqual(new Point(1, 3), uneEllipse.PointDepart);
                Assert.AreEqual(new Point(2, 5), uneEllipse.PointFin);
                Assert.AreEqual(1, uneEllipse.LargeurTrait);
                Assert.AreEqual("La couleur de bordure est nulle.", e.Message);
            }
        }

        [Test]
        public void TesterAccesseurCouleurRemplissageEnEcriture()
        {
            Point pointDepart = new Point(1, 3);
            Point pointFin = new Point(2, 5);
            Ellipse uneEllipse = new Ellipse(Color.Black, Color.Blue, pointDepart, pointFin, 1)
            {
                CouleurRemplissage = Color.Aqua
            };
            Assert.AreEqual(Color.Black, uneEllipse.CouleurBordure);
            Assert.AreEqual(Color.Aqua, uneEllipse.CouleurRemplissage);
            Assert.AreEqual(new Point(1, 3), uneEllipse.PointDepart);
            Assert.AreEqual(new Point(2, 5), uneEllipse.PointFin);
            Assert.AreEqual(1, uneEllipse.LargeurTrait);
            try
            {
                uneEllipse.CouleurRemplissage = Color.Empty;
                Assert.Fail();
            }
            catch (ArgumentNullException e)
            {
                Assert.AreEqual(Color.Black, uneEllipse.CouleurBordure);
                Assert.AreEqual(Color.Aqua, uneEllipse.CouleurRemplissage);
                Assert.AreEqual(new Point(1, 3), uneEllipse.PointDepart);
                Assert.AreEqual(new Point(2, 5), uneEllipse.PointFin);
                Assert.AreEqual(1, uneEllipse.LargeurTrait);
                Assert.AreEqual("La couleur de remplissage est nulle.", e.Message);
            }
        }
    }
}