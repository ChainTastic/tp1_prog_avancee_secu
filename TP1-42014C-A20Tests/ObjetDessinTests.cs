using NUnit.Framework;
using System;
using System.Drawing;
using Tp1_42014C_A20;
using Rectangle = Tp1_42014C_A20.Rectangle;

namespace TP1_42014C_A20Tests
{
    public class ObjetDessinTests
    {
        [SetUp]
        public void Setup()
        {
        }

        [Test]
        public void ConstructeurTestAvecPointDeDepartInvalide()
        {
            Point pointFin = new Point(2, 5);
            Ellipse uneEllipse = null;

            try
            {
                uneEllipse = new Ellipse(Color.Blue, Color.Black, Point.Empty, pointFin, 1);
                Assert.Fail();
            }
            catch (ArgumentNullException e)
            {
                Assert.IsNull(uneEllipse);
                Assert.AreEqual("Le point de d�part est nul.", e.Message);
            }

            try
            {
                uneEllipse = new Ellipse(Color.Blue, Color.Black, Point.Empty, Point.Empty, 1);
                Assert.Fail();
            }
            catch (ArgumentNullException e)
            {
                Assert.IsNull(uneEllipse);
                Assert.AreEqual("Le point de d�part est nul.", e.Message);
            }

            try
            {
                uneEllipse = new Ellipse(Color.Blue, Color.Black, Point.Empty, pointFin, 1);
                Assert.Fail();
            }
            catch (ArgumentNullException e)
            {
                Assert.IsNull(uneEllipse);
                Assert.AreEqual("Le point de d�part est nul.", e.Message);
            }
        }

        [Test]
        public void ConstructeurTestAvecPointDeFinInvalide()
        {
            Point pointDepart = new Point(1, 3);
            Ellipse uneEllipse = null;

            try
            {
                uneEllipse = new Ellipse(Color.Blue, Color.Blue, pointDepart, Point.Empty, 1);
                Assert.Fail();
            }
            catch (ArgumentNullException e)
            {
                Assert.IsNull(uneEllipse);
                Assert.AreEqual("Le point de fin est nul.", e.Message);
            }
        }

        [Test]
        public void LargeurTraitEgaleOuSousZero()
        {
            Assert.Throws<ArgumentException>(ExceptionLargeurTraitEgaleZero);
            Assert.Throws<ArgumentException>(ExceptionLargeurTraitSousZero);
        }

        [Test]
        public void TesterAccesseurLargeurTraitEnEcriture()
        {
            Point pointDepart = new Point(1, 3);
            Point pointFin = new Point(2, 5);
            ObjetDessin unObjetDessin = new Ligne(Color.Black, pointDepart, pointFin, 1);

            unObjetDessin.LargeurTrait = 5;
            Assert.AreEqual(new Point(1, 3), unObjetDessin.PointDepart);
            Assert.AreEqual(new Point(2, 5), unObjetDessin.PointFin);
            Assert.AreEqual(5, unObjetDessin.LargeurTrait);

            try
            {
                unObjetDessin.LargeurTrait = 0;
                Assert.Fail();
            }
            catch (ArgumentException ex)
            {
                Assert.AreEqual(new Point(1, 3), unObjetDessin.PointDepart);
                Assert.AreEqual(new Point(2, 5), unObjetDessin.PointFin);
                Assert.AreEqual(5, unObjetDessin.LargeurTrait);
                Assert.AreEqual("La largeur du trait ne peut �tre de 0 et moins.",
                    ex.Message);
            }

            try
            {
                unObjetDessin.LargeurTrait = -5;
                Assert.Fail();
            }
            catch (ArgumentException ex)
            {
                Assert.AreEqual(new Point(1, 3), unObjetDessin.PointDepart);
                Assert.AreEqual(new Point(2, 5), unObjetDessin.PointFin);
                Assert.AreEqual(5, unObjetDessin.LargeurTrait);
                Assert.AreEqual("La largeur du trait ne peut �tre de 0 et moins.",
                    ex.Message);
            }
        }

        [Test]
        public void TesterAccesseurPointDepartEnEcriture()
        {
            Point pointDepart = new Point(1, 3);
            Point pointFin = new Point(2, 5);
            ObjetDessin unObjetDessin = new Ligne(Color.Black, pointDepart, pointFin, 1);

            unObjetDessin.PointDepart = new Point(1,6);
            Assert.AreEqual(new Point(1, 6), unObjetDessin.PointDepart);
            Assert.AreEqual(new Point(2, 5), unObjetDessin.PointFin);
            Assert.AreEqual(1, unObjetDessin.LargeurTrait);

            try
            {
                unObjetDessin.PointDepart = Point.Empty;
                Assert.Fail();
            }
            catch (ArgumentNullException ex)
            {
                Assert.AreEqual(new Point(1, 6), unObjetDessin.PointDepart);
                Assert.AreEqual(new Point(2, 5), unObjetDessin.PointFin);
                Assert.AreEqual(1, unObjetDessin.LargeurTrait);
                Assert.AreEqual("Le point de d�part est nul.",
                    ex.Message);
            }
        }

        [Test]
        public void TesterAccesseurPointFinEnEcriture()
        {
            Point pointDepart = new Point(1, 3);
            Point pointFin = new Point(2, 5);
            ObjetDessin unObjetDessin = new Ligne(Color.Black, pointDepart, pointFin, 1);

            unObjetDessin.PointFin = new Point(2, 6);
            Assert.AreEqual(new Point(1, 3), unObjetDessin.PointDepart);
            Assert.AreEqual(new Point(2, 6), unObjetDessin.PointFin);
            Assert.AreEqual(1, unObjetDessin.LargeurTrait);

            try
            {
                unObjetDessin.PointFin = Point.Empty;
                Assert.Fail();
            }
            catch (ArgumentNullException ex)
            {
                Assert.AreEqual(new Point(1, 3), unObjetDessin.PointDepart);
                Assert.AreEqual(new Point(2, 6), unObjetDessin.PointFin);
                Assert.AreEqual(1, unObjetDessin.LargeurTrait);
                Assert.AreEqual("Le point de fin est nul.",
                    ex.Message);
            }
        }

        private void ExceptionLargeurTraitEgaleZero()
        {
            Point pointDepart = new Point(1, 3);
            Point pointFin = new Point(2, 5);
            ObjetDessin unObjetDessin = new Rectangle(Color.Black, Color.Black, pointDepart, pointFin, 0);
        }

        private void ExceptionLargeurTraitSousZero()
        {
            Point pointDepart = new Point(1, 3);
            Point pointFin = new Point(2, 5);
            ObjetDessin unObjetDessin = new Rectangle(Color.Black, Color.Black, pointDepart, pointFin, -10);
        }
    }
}