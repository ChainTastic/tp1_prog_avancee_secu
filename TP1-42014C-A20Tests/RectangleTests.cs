using NUnit.Framework;
using System.Drawing;

namespace TP1_42014C_A20Tests
{
    public class RectangleTests
    {
        [SetUp]
        public void Setup()
        {
        }

        [Test]
        public void ConstructeurTestAvecParametresValides()
        {
            Point pointDepart = new Point(1, 3);
            Point pointFin = new Point(2, 5);
            Tp1_42014C_A20.Rectangle unRectangle =
                new Tp1_42014C_A20.Rectangle(Color.Black, Color.Blue, pointDepart, pointFin, 1);
            Assert.AreEqual(Color.Black, unRectangle.CouleurBordure);
            Assert.AreEqual(Color.Blue, unRectangle.CouleurRemplissage);
            Assert.AreEqual(pointDepart, unRectangle.PointDepart);
            Assert.AreEqual(pointFin, unRectangle.PointFin);
            Assert.AreEqual(1, unRectangle.LargeurTrait);
        }

        [Test]
        public void CalculerAireRectangleTests()
        {
            Point pointDepart = new Point(1, 3);
            Point pointFin = new Point(2, 5);
            Tp1_42014C_A20.Rectangle unRectangle =
                new Tp1_42014C_A20.Rectangle(Color.Black, Color.Blue, pointDepart, pointFin, 1);
            double hauteur = unRectangle.PointFin.Y - unRectangle.PointDepart.Y;
            double largeur = unRectangle.PointFin.X - unRectangle.PointDepart.X;
            Assert.AreEqual(largeur * hauteur, unRectangle.CalculerAire(hauteur, largeur));
        }
    }
}