using NUnit.Framework;
using System;
using System.Drawing;
using Tp1_42014C_A20;

namespace TP1_42014C_A20Tests
{
    public class EllipseTests
    {
        [SetUp]
        public void Setup()
        {
        }

        [Test]
        public void ConstructeurTestAvecParametresValides()
        {
            Point pointDepart = new Point(1, 3);
            Point pointFin = new Point(2, 5);
            Ellipse unEllipse = new Ellipse(Color.Blue, Color.Black, pointDepart, pointFin, 1);
            Assert.AreEqual(Color.Blue, unEllipse.CouleurBordure);
            Assert.AreEqual(Color.Black, unEllipse.CouleurRemplissage);
            Assert.AreEqual(pointDepart, unEllipse.PointDepart);
            Assert.AreEqual(pointFin, unEllipse.PointFin);
            Assert.AreEqual(1, unEllipse.LargeurTrait);
        }


        [Test]
        public void CalculerAireEllipseTests()
        {
            Point pointDepart = new Point(1, 3);
            Point pointFin = new Point(2, 5);
            Ellipse unEllipse = new Ellipse(Color.Blue, Color.Black, pointDepart, pointFin, 1);
            double hauteur = unEllipse.PointFin.Y - unEllipse.PointDepart.Y;
            double largeur = unEllipse.PointFin.X - unEllipse.PointDepart.X;
            Assert.AreEqual(Math.PI * (hauteur / 2) * (largeur / 2), unEllipse.CalculerAire(hauteur, largeur));
        }
    }
}