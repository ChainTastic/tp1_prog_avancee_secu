﻿using System;
using System.Drawing;

namespace Tp1_42014C_A20
{
    public abstract class Forme : ObjetDessin
    {
        #region Attributs

        private Color _couleurBordure;

        private Color _couleurRemplissage;

        #endregion Attributs

        #region Get/Set

        public Color CouleurBordure
        {
            get { return _couleurBordure; }
            set
            {
                if (value == Color.Empty)
                {
                    throw new ArgumentNullException("La couleur de bordure est nulle.", new ArgumentNullException());
                }

                _couleurBordure = value;
            }
        }

        public Color CouleurRemplissage
        {
            get { return _couleurRemplissage; }
            set
            {
                if (value == Color.Empty)
                {
                    throw new ArgumentNullException("La couleur de remplissage est nulle.",
                        new ArgumentNullException());
                }

                _couleurRemplissage = value;
            }
        }

        #endregion Get/Set

        #region Constructeurs

        protected Forme(Point pPointDepart, Point pPointFin, int pLargeurTrait, Color pCouleurBordure,
            Color pCouleurRemplissage) : base(pPointDepart, pPointFin, pLargeurTrait)
        {
            CouleurBordure = pCouleurBordure;
            CouleurRemplissage = pCouleurRemplissage;
        }

        #endregion Constructeurs

        #region Méthodes

        /// <summary>
        /// Méthode permettant de calculer l'aire de la forme
        /// </summary>
        /// <param name="pHauteur">La hauteur de la forme</param>
        /// <param name="pLargeur">La largeur de la forme</param>
        /// <returns>L'aire de la forme</returns>
        public abstract double CalculerAire(double pHauteur, double pLargeur);

        #endregion Méthodes
    }
}