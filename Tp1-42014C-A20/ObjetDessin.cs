﻿using System;
using System.Drawing;

namespace Tp1_42014C_A20
{
    public abstract class ObjetDessin
    {
        #region Attributs

        private int _largeurTrait;

        private Point _pointDepart;

        private Point _pointFin;

        #endregion Attributs

        #region Get/Set

        public int LargeurTrait
        {
            get { return _largeurTrait; }
            set
            {
                if (value <= 0)
                {
                    throw new ArgumentException("La largeur du trait ne peut être de 0 et moins.");
                }

                _largeurTrait = value;
            }
        }

        public Point PointDepart
        {
            get { return _pointDepart; }
            set
            {
                if (value == Point.Empty)
                {
                    throw new ArgumentNullException("Le point de départ est nul.", new ArgumentNullException());
                }

                _pointDepart = value;
            }
        }

        public Point PointFin
        {
            get { return _pointFin; }
            set
            {
                if (value == Point.Empty)
                {
                    throw new ArgumentNullException("Le point de fin est nul.", new ArgumentNullException());
                }

                _pointFin = value;
            }
        }

        #endregion Get/Set

        #region Constructeurs

        protected ObjetDessin(Point pPointDepart, Point pPointFin, int pLargeurTrait)
        {
            PointDepart = pPointDepart;
            PointFin = pPointFin;
            LargeurTrait = pLargeurTrait;
        }

        #endregion Constructeurs

        #region Méthodes

        /// <summary>
        /// Méthode qui permet d'afficher un objet dessin dans l'élément graphique passé en paramètre.
        /// </summary>
        /// <param name="pGraphics">L'élément graphique dans lequel afficher la ligne</param>
        public abstract void Afficher(Graphics pGraphics);

        #endregion Méthodes
    }
}