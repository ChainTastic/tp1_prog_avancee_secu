﻿using System.Drawing;

namespace Tp1_42014C_A20
{
    public class Rectangle : Forme
    {
        #region Constructeurs

        public Rectangle(Color pCouleurBordure,
            Color pCouleurRemplissage, Point pPointDepart, Point pPointFin, int pLargeurTrait) : base(pPointDepart,
            pPointFin, pLargeurTrait, pCouleurBordure,
            pCouleurRemplissage)
        {
        }

        #endregion Constructeurs

        #region Méthodes

        /// <summary>
        /// Méthode qui permet d'afficher le rectangle dans l'élément graphique passé en paramètre.
        /// </summary>
        /// <param name="pGraphics">L'élément graphique dans lequel afficher le rectangle</param>
        public override void Afficher(Graphics pGraphics)
        {
            Size tailleRectangle = new Size(PointFin.X - PointDepart.X, PointFin.Y - PointDepart.Y);
            pGraphics.FillRectangle(new SolidBrush(CouleurRemplissage),
                new System.Drawing.Rectangle(PointDepart, tailleRectangle));
            pGraphics.DrawRectangle(new Pen(CouleurBordure, LargeurTrait),
                new System.Drawing.Rectangle(PointDepart, tailleRectangle));
        }

        #region Overrides of Forme

        /// <summary>
        /// Méthode permettant de calculer l'aire dun rectangle
        /// </summary>
        /// <param name="pHauteur">La hauteur du rectangle</param>
        /// <param name="pLargeur">La largeur du rectangle</param>
        /// <returns>L'aire du rectangle</returns>
        public override double CalculerAire(double pHauteur, double pLargeur)
        {
            return pHauteur * pLargeur;
        }

        #endregion Overrides of Forme

        #endregion Méthodes
    }
}