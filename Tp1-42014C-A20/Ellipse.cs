﻿using System;
using System.Drawing;

namespace Tp1_42014C_A20
{
    public class Ellipse : Forme
    {
        #region Constructeurs

        public Ellipse(Color pCouleurBordure,
            Color pCouleurRemplissage, Point pPointDepart, Point pPointFin, int pLargeurTrait) : base(pPointDepart,
            pPointFin, pLargeurTrait, pCouleurBordure,
            pCouleurRemplissage)
        {
        }

        #endregion Constructeurs

        #region Méthodes

        /// <summary>
        /// Méthode qui permet d'afficher l'ellipse dans l'élément graphique passé en paramètre.
        /// </summary>
        /// <param name="pGraphics">L'élément graphique dans lequel afficher l'ellipse</param>
        public override void Afficher(Graphics pGraphics)
        {
            Size tailleEllipse = new Size(PointFin.X - PointDepart.X, PointFin.Y - PointDepart.Y);
            pGraphics.FillEllipse(new SolidBrush(CouleurRemplissage),
                new System.Drawing.Rectangle(PointDepart, tailleEllipse));
            pGraphics.DrawEllipse(new Pen(CouleurBordure, LargeurTrait),
                new System.Drawing.Rectangle(PointDepart, tailleEllipse));
        }

        /// <summary>
        /// Méthode qui permet de calculer l'aire d'une ellipse. Merci internet :)
        /// Pour plus d'info, voir un prof de math!
        /// </summary>
        /// <param name="pHauteur">La hauteur de l'ellipse</param>
        /// <param name="pLargeur">La largeur de l'ellipse</param>
        /// <returns>L'aire de l'ellipse</returns>
        public override double CalculerAire(double pHauteur, double pLargeur)
        {
            return Math.PI * (pHauteur / 2) * (pLargeur / 2);
        }

        #endregion Méthodes
    }
}