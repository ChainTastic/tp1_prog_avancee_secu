﻿using System;
using System.Drawing;

namespace Tp1_42014C_A20
{
    public class Ligne : ObjetDessin
    {
        #region Attributs

        private Color _couleur;

        #endregion Attributs

        #region Get/Set

        public Color Couleur
        {
            get { return _couleur; }
            set
            {
                if (value == Color.Empty)
                {
                    throw new ArgumentNullException("La couleur est nulle.", new ArgumentNullException());
                }

                _couleur = value;
            }
        }

        #endregion Get/Set

        #region Constructeurs

        public Ligne(Color pCouleur, Point pPointDepart, Point pPointFin, int pLargeurTrait) : base(pPointDepart,
            pPointFin, pLargeurTrait)
        {
            Couleur = pCouleur;
        }

        #endregion Constructeurs

        #region Méthodes

        #region Overrides of ObjetDessin

        /// <summary>
        /// Méthode qui permet d'afficher la ligne dans l'élément graphique passé en paramètre.
        /// </summary>
        /// <param name="pGraphics">L'élément graphique dans lequel afficher la ligne</param>
        public override void Afficher(Graphics pGraphics)
        {
            pGraphics.DrawLine(new Pen(Couleur, LargeurTrait), PointDepart, PointFin);
        }

        #endregion Overrides of ObjetDessin

        #endregion Méthodes
    }
}