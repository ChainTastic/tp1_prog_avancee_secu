﻿namespace Tp1_42014C_A20
{
    partial class frmAccueil
    {
        /// <summary>
        ///  Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        ///  Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        ///  Required method for Designer support - do not modify
        ///  the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.pbDessin = new System.Windows.Forms.PictureBox();
            this.btnCouleurRemplissage = new System.Windows.Forms.Button();
            this.couleurRemplissage = new System.Windows.Forms.ColorDialog();
            this.couleurBordure = new System.Windows.Forms.ColorDialog();
            this.btnCouleurBordure = new System.Windows.Forms.Button();
            this.lblRemplissage = new System.Windows.Forms.Label();
            this.lblBordure = new System.Windows.Forms.Label();
            this.gbCouleur = new System.Windows.Forms.GroupBox();
            this.rbLigne = new System.Windows.Forms.RadioButton();
            this.gbFormes = new System.Windows.Forms.GroupBox();
            this.rbEllipse = new System.Windows.Forms.RadioButton();
            this.rbRectangle = new System.Windows.Forms.RadioButton();
            this.gbTrait = new System.Windows.Forms.GroupBox();
            this.cmbTrait = new System.Windows.Forms.ComboBox();
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.menuEdition = new System.Windows.Forms.ToolStripMenuItem();
            this.menuAnnuler = new System.Windows.Forms.ToolStripMenuItem();
            this.menuRepeter = new System.Windows.Forms.ToolStripMenuItem();
            this.errorProvider1 = new System.Windows.Forms.ErrorProvider(this.components);
            this.gbCalculPeinture = new System.Windows.Forms.GroupBox();
            this.btnCalculer = new System.Windows.Forms.Button();
            this.lblLargeur = new System.Windows.Forms.Label();
            this.lblHauteur = new System.Windows.Forms.Label();
            this.txtLargeur = new System.Windows.Forms.TextBox();
            this.txtHauteur = new System.Windows.Forms.TextBox();
            this.lbResultatsCalculPeinture = new System.Windows.Forms.ListBox();
            ((System.ComponentModel.ISupportInitialize)(this.pbDessin)).BeginInit();
            this.gbCouleur.SuspendLayout();
            this.gbFormes.SuspendLayout();
            this.gbTrait.SuspendLayout();
            this.menuStrip1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.errorProvider1)).BeginInit();
            this.gbCalculPeinture.SuspendLayout();
            this.SuspendLayout();
            // 
            // pbDessin
            // 
            this.pbDessin.BackColor = System.Drawing.Color.White;
            this.pbDessin.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pbDessin.Location = new System.Drawing.Point(10, 124);
            this.pbDessin.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.pbDessin.Name = "pbDessin";
            this.pbDessin.Size = new System.Drawing.Size(525, 450);
            this.pbDessin.TabIndex = 0;
            this.pbDessin.TabStop = false;
            this.pbDessin.Paint += new System.Windows.Forms.PaintEventHandler(this.pbDessin_Paint);
            this.pbDessin.MouseDown += new System.Windows.Forms.MouseEventHandler(this.pbDessin_MouseDown);
            this.pbDessin.MouseUp += new System.Windows.Forms.MouseEventHandler(this.pbDessin_MouseUp);
            // 
            // btnCouleurRemplissage
            // 
            this.btnCouleurRemplissage.Location = new System.Drawing.Point(13, 34);
            this.btnCouleurRemplissage.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.btnCouleurRemplissage.Name = "btnCouleurRemplissage";
            this.btnCouleurRemplissage.Size = new System.Drawing.Size(81, 52);
            this.btnCouleurRemplissage.TabIndex = 1;
            this.btnCouleurRemplissage.UseVisualStyleBackColor = true;
            this.btnCouleurRemplissage.Click += new System.EventHandler(this.btnCouleur_Click);
            // 
            // btnCouleurBordure
            // 
            this.btnCouleurBordure.Location = new System.Drawing.Point(100, 34);
            this.btnCouleurBordure.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.btnCouleurBordure.Name = "btnCouleurBordure";
            this.btnCouleurBordure.Size = new System.Drawing.Size(81, 52);
            this.btnCouleurBordure.TabIndex = 1;
            this.btnCouleurBordure.UseVisualStyleBackColor = true;
            this.btnCouleurBordure.Click += new System.EventHandler(this.btnCouleurBordure_Click);
            // 
            // lblRemplissage
            // 
            this.lblRemplissage.AutoSize = true;
            this.lblRemplissage.Location = new System.Drawing.Point(13, 16);
            this.lblRemplissage.Name = "lblRemplissage";
            this.lblRemplissage.Size = new System.Drawing.Size(73, 15);
            this.lblRemplissage.TabIndex = 2;
            this.lblRemplissage.Text = "Remplissage";
            // 
            // lblBordure
            // 
            this.lblBordure.AutoSize = true;
            this.lblBordure.Location = new System.Drawing.Point(109, 16);
            this.lblBordure.Name = "lblBordure";
            this.lblBordure.Size = new System.Drawing.Size(49, 15);
            this.lblBordure.TabIndex = 3;
            this.lblBordure.Text = "Bordure";
            // 
            // gbCouleur
            // 
            this.gbCouleur.Controls.Add(this.btnCouleurRemplissage);
            this.gbCouleur.Controls.Add(this.btnCouleurBordure);
            this.gbCouleur.Controls.Add(this.lblBordure);
            this.gbCouleur.Controls.Add(this.lblRemplissage);
            this.gbCouleur.Location = new System.Drawing.Point(10, 23);
            this.gbCouleur.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.gbCouleur.Name = "gbCouleur";
            this.gbCouleur.Padding = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.gbCouleur.Size = new System.Drawing.Size(196, 97);
            this.gbCouleur.TabIndex = 5;
            this.gbCouleur.TabStop = false;
            this.gbCouleur.Text = "Couleurs";
            // 
            // rbLigne
            // 
            this.rbLigne.AutoSize = true;
            this.rbLigne.Location = new System.Drawing.Point(18, 20);
            this.rbLigne.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.rbLigne.Name = "rbLigne";
            this.rbLigne.Size = new System.Drawing.Size(54, 19);
            this.rbLigne.TabIndex = 4;
            this.rbLigne.TabStop = true;
            this.rbLigne.Text = "Ligne";
            this.rbLigne.UseVisualStyleBackColor = true;
            // 
            // gbFormes
            // 
            this.gbFormes.Controls.Add(this.rbEllipse);
            this.gbFormes.Controls.Add(this.rbRectangle);
            this.gbFormes.Controls.Add(this.rbLigne);
            this.gbFormes.Location = new System.Drawing.Point(212, 26);
            this.gbFormes.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.gbFormes.Name = "gbFormes";
            this.gbFormes.Padding = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.gbFormes.Size = new System.Drawing.Size(128, 94);
            this.gbFormes.TabIndex = 6;
            this.gbFormes.TabStop = false;
            this.gbFormes.Text = "Formes";
            // 
            // rbEllipse
            // 
            this.rbEllipse.AutoSize = true;
            this.rbEllipse.Location = new System.Drawing.Point(18, 64);
            this.rbEllipse.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.rbEllipse.Name = "rbEllipse";
            this.rbEllipse.Size = new System.Drawing.Size(58, 19);
            this.rbEllipse.TabIndex = 4;
            this.rbEllipse.TabStop = true;
            this.rbEllipse.Text = "Ellipse";
            this.rbEllipse.UseVisualStyleBackColor = true;
            // 
            // rbRectangle
            // 
            this.rbRectangle.AutoSize = true;
            this.rbRectangle.Location = new System.Drawing.Point(18, 42);
            this.rbRectangle.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.rbRectangle.Name = "rbRectangle";
            this.rbRectangle.Size = new System.Drawing.Size(77, 19);
            this.rbRectangle.TabIndex = 4;
            this.rbRectangle.TabStop = true;
            this.rbRectangle.Text = "Rectangle";
            this.rbRectangle.UseVisualStyleBackColor = true;
            // 
            // gbTrait
            // 
            this.gbTrait.Controls.Add(this.cmbTrait);
            this.gbTrait.Location = new System.Drawing.Point(345, 26);
            this.gbTrait.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.gbTrait.Name = "gbTrait";
            this.gbTrait.Padding = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.gbTrait.Size = new System.Drawing.Size(128, 60);
            this.gbTrait.TabIndex = 6;
            this.gbTrait.TabStop = false;
            this.gbTrait.Text = "Trait";
            // 
            // cmbTrait
            // 
            this.cmbTrait.FormattingEnabled = true;
            this.cmbTrait.Items.AddRange(new object[] {
            "1",
            "2",
            "3",
            "4",
            "5",
            "6",
            "7",
            "8",
            "9",
            "10"});
            this.cmbTrait.Location = new System.Drawing.Point(5, 20);
            this.cmbTrait.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.cmbTrait.Name = "cmbTrait";
            this.cmbTrait.Size = new System.Drawing.Size(118, 23);
            this.cmbTrait.TabIndex = 0;
            this.cmbTrait.Text = "1";
            // 
            // menuStrip1
            // 
            this.menuStrip1.ImageScalingSize = new System.Drawing.Size(20, 20);
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.menuEdition});
            this.menuStrip1.Location = new System.Drawing.Point(0, 0);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Padding = new System.Windows.Forms.Padding(5, 2, 0, 2);
            this.menuStrip1.Size = new System.Drawing.Size(814, 24);
            this.menuStrip1.TabIndex = 7;
            this.menuStrip1.Text = "menuStrip1";
            this.menuStrip1.MouseDown += new System.Windows.Forms.MouseEventHandler(this.pbDessin_MouseDown);
            // 
            // menuEdition
            // 
            this.menuEdition.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.menuAnnuler,
            this.menuRepeter});
            this.menuEdition.Name = "menuEdition";
            this.menuEdition.Size = new System.Drawing.Size(56, 20);
            this.menuEdition.Text = "Édition";
            // 
            // menuAnnuler
            // 
            this.menuAnnuler.Name = "menuAnnuler";
            this.menuAnnuler.Size = new System.Drawing.Size(116, 22);
            this.menuAnnuler.Text = "Annuler";
            this.menuAnnuler.Click += new System.EventHandler(this.menuAnnuler_Click);
            // 
            // menuRepeter
            // 
            this.menuRepeter.Name = "menuRepeter";
            this.menuRepeter.Size = new System.Drawing.Size(116, 22);
            this.menuRepeter.Text = "Répéter";
            this.menuRepeter.Click += new System.EventHandler(this.menuRepeter_Click);
            // 
            // errorProvider1
            // 
            this.errorProvider1.BlinkStyle = System.Windows.Forms.ErrorBlinkStyle.NeverBlink;
            this.errorProvider1.ContainerControl = this;
            // 
            // gbCalculPeinture
            // 
            this.gbCalculPeinture.Controls.Add(this.btnCalculer);
            this.gbCalculPeinture.Controls.Add(this.lblLargeur);
            this.gbCalculPeinture.Controls.Add(this.lblHauteur);
            this.gbCalculPeinture.Controls.Add(this.txtLargeur);
            this.gbCalculPeinture.Controls.Add(this.txtHauteur);
            this.gbCalculPeinture.Location = new System.Drawing.Point(558, 26);
            this.gbCalculPeinture.Name = "gbCalculPeinture";
            this.gbCalculPeinture.Size = new System.Drawing.Size(244, 101);
            this.gbCalculPeinture.TabIndex = 8;
            this.gbCalculPeinture.TabStop = false;
            this.gbCalculPeinture.Text = "Calcul peinture";
            // 
            // btnCalculer
            // 
            this.btnCalculer.Location = new System.Drawing.Point(47, 78);
            this.btnCalculer.Name = "btnCalculer";
            this.btnCalculer.Size = new System.Drawing.Size(155, 23);
            this.btnCalculer.TabIndex = 10;
            this.btnCalculer.Text = "Calculer";
            this.btnCalculer.UseVisualStyleBackColor = true;
            this.btnCalculer.Click += new System.EventHandler(this.btnCalculer_Click);
            // 
            // lblLargeur
            // 
            this.lblLargeur.AutoSize = true;
            this.lblLargeur.Location = new System.Drawing.Point(6, 54);
            this.lblLargeur.Name = "lblLargeur";
            this.lblLargeur.Size = new System.Drawing.Size(47, 15);
            this.lblLargeur.TabIndex = 9;
            this.lblLargeur.Text = "Largeur";
            // 
            // lblHauteur
            // 
            this.lblHauteur.AutoSize = true;
            this.lblHauteur.Location = new System.Drawing.Point(5, 27);
            this.lblHauteur.Name = "lblHauteur";
            this.lblHauteur.Size = new System.Drawing.Size(50, 15);
            this.lblHauteur.TabIndex = 9;
            this.lblHauteur.Text = "Hauteur";
            // 
            // txtLargeur
            // 
            this.txtLargeur.Location = new System.Drawing.Point(61, 51);
            this.txtLargeur.Name = "txtLargeur";
            this.txtLargeur.Size = new System.Drawing.Size(64, 23);
            this.txtLargeur.TabIndex = 0;
            // 
            // txtHauteur
            // 
            this.txtHauteur.Location = new System.Drawing.Point(61, 22);
            this.txtHauteur.Name = "txtHauteur";
            this.txtHauteur.Size = new System.Drawing.Size(64, 23);
            this.txtHauteur.TabIndex = 0;
            // 
            // lbResultatsCalculPeinture
            // 
            this.lbResultatsCalculPeinture.FormattingEnabled = true;
            this.lbResultatsCalculPeinture.ItemHeight = 15;
            this.lbResultatsCalculPeinture.Location = new System.Drawing.Point(558, 133);
            this.lbResultatsCalculPeinture.Name = "lbResultatsCalculPeinture";
            this.lbResultatsCalculPeinture.Size = new System.Drawing.Size(244, 439);
            this.lbResultatsCalculPeinture.TabIndex = 9;
            // 
            // frmAccueil
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 15F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(814, 584);
            this.Controls.Add(this.lbResultatsCalculPeinture);
            this.Controls.Add(this.gbCalculPeinture);
            this.Controls.Add(this.gbTrait);
            this.Controls.Add(this.gbFormes);
            this.Controls.Add(this.gbCouleur);
            this.Controls.Add(this.pbDessin);
            this.Controls.Add(this.menuStrip1);
            this.MainMenuStrip = this.menuStrip1;
            this.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.Name = "frmAccueil";
            this.Text = "Art guerilla";
            ((System.ComponentModel.ISupportInitialize)(this.pbDessin)).EndInit();
            this.gbCouleur.ResumeLayout(false);
            this.gbCouleur.PerformLayout();
            this.gbFormes.ResumeLayout(false);
            this.gbFormes.PerformLayout();
            this.gbTrait.ResumeLayout(false);
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.errorProvider1)).EndInit();
            this.gbCalculPeinture.ResumeLayout(false);
            this.gbCalculPeinture.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.PictureBox pbDessin;
        private System.Windows.Forms.Button btnCouleurRemplissage;
        private System.Windows.Forms.ColorDialog couleurRemplissage;
        private System.Windows.Forms.ColorDialog couleurBordure;
        private System.Windows.Forms.Button btnCouleurBordure;
        private System.Windows.Forms.Label lblRemplissage;
        private System.Windows.Forms.Label lblBordure;
        private System.Windows.Forms.GroupBox gbCouleur;
        private System.Windows.Forms.RadioButton rbLigne;
        private System.Windows.Forms.GroupBox gbFormes;
        private System.Windows.Forms.RadioButton rbRectangle;
        private System.Windows.Forms.RadioButton rbEllipse;
        private System.Windows.Forms.GroupBox gbTrait;
        private System.Windows.Forms.ComboBox cmbTrait;
        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.ToolStripMenuItem menuEdition;
        private System.Windows.Forms.ToolStripMenuItem menuAnnuler;
        private System.Windows.Forms.ToolStripMenuItem menuRepeter;
        private System.Windows.Forms.ErrorProvider errorProvider1;
        private System.Windows.Forms.GroupBox gbCalculPeinture;
        private System.Windows.Forms.Button btnCalculer;
        private System.Windows.Forms.Label lblLargeur;
        private System.Windows.Forms.Label lblHauteur;
        private System.Windows.Forms.TextBox txtLargeur;
        private System.Windows.Forms.TextBox txtHauteur;
        private System.Windows.Forms.ListBox lbResultatsCalculPeinture;
    }
}

