﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Windows.Forms;

namespace Tp1_42014C_A20
{
    public partial class frmAccueil : Form
    {
        #region Attributs

        private Boolean _bDessinEnCours;
        private Point _pntPositionDepart;
        private Stack<ObjetDessin> _pileObjetDessins;
        private Stack<ObjetDessin> _pileDessinsAnnule;

        private const float largeurCanvas = 600;
        private const float hauteurCanvas = 600;

        #endregion Attributs

        #region Constructeurs

        public frmAccueil()
        {
            InitializeComponent();

            _bDessinEnCours = false;

            couleurRemplissage.Color = Color.Gray;
            btnCouleurRemplissage.BackColor = couleurRemplissage.Color;
            couleurBordure.Color = Color.Black;
            btnCouleurBordure.BackColor = couleurBordure.Color;

            _pileObjetDessins = new Stack<ObjetDessin>();
            _pileDessinsAnnule = new Stack<ObjetDessin>();
        }

        #endregion Constructeurs

        #region Événements

        /// <summary>
        /// Évenement lorsque l'utilisateur appuie sur le bouton "Couleur".
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnCouleur_Click(object sender, EventArgs e)
        {
            DialogResult dr = couleurRemplissage.ShowDialog();
            if (dr == DialogResult.OK)
            {
                btnCouleurRemplissage.BackColor = couleurRemplissage.Color;
            }
        }

        /// <summary>
        /// Évenement lorsque l'utilisateur appuie sur le bouton "Couleur Bordure".
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnCouleurBordure_Click(object sender, EventArgs e)
        {
            DialogResult dr = couleurBordure.ShowDialog();
            if (dr == DialogResult.OK)
            {
                btnCouleurBordure.BackColor = couleurBordure.Color;
            }
        }

        /// <summary>
        /// Évenemnt lorsque l'utilisateur appuie sur le bouton gauche de la souris sur le picturebox.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void pbDessin_MouseDown(object sender, MouseEventArgs e)
        {
            _bDessinEnCours = true;
            _pntPositionDepart = new Point(e.X, e.Y);
        }

        /// <summary>
        /// Évenement lorsque le picturebox affiche les dessins de l'utilisateur
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void pbDessin_Paint(object sender, PaintEventArgs e)
        {
            foreach (ObjetDessin dessin in _pileObjetDessins)
            {
                dessin.Afficher(e.Graphics);
            }
        }

        /// <summary>
        /// Évenement lorsque l'utilisateur relache le bouton gauche de la souris sur le picturebox.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void pbDessin_MouseUp(object sender, MouseEventArgs e)
        {
            if (_bDessinEnCours)
            {
                if (rbLigne.Checked)
                {
                    DessinerLigne(e);
                }
                else
                {
                    Point hautGauche = new Point(Math.Min(_pntPositionDepart.X, e.X),
                        Math.Min(_pntPositionDepart.Y, e.Y));
                    Point basDroite = new Point(Math.Max(_pntPositionDepart.X, e.X),
                        Math.Max(_pntPositionDepart.Y, e.Y));
                    if (rbRectangle.Checked)
                    {
                        DessinerRectangle(hautGauche, basDroite);
                    }
                    else
                    {
                        DessinerEllipse(hautGauche, basDroite);
                    }
                }

                _bDessinEnCours = false;
                pbDessin.Refresh();
            }
        }

        /// <summary>
        /// Évenement lorsque l'utilisateur appuie sur l'option "Annuler".
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void menuAnnuler_Click(object sender, EventArgs e)
        {
            if (_pileObjetDessins.Count > 0)
            {
                _pileDessinsAnnule.Push(_pileObjetDessins.Pop());
                pbDessin.Refresh();
            }
        }

        /// <summary>
        /// Évenement lorsque l'utilisateur appuie sur l'option "Répéter".
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void menuRepeter_Click(object sender, EventArgs e)
        {
            if (_pileDessinsAnnule.Count > 0)
            {
                _pileObjetDessins.Push(_pileDessinsAnnule.Pop());
                pbDessin.Refresh();
            }
        }

        /// <summary>
        /// Évenement lorsque l'utilisateur appuie sur le bouton "Calculer".
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnCalculer_Click(object sender, EventArgs e)
        {
            this.errorProvider1.Clear();
            if (ValiderChamp(this.txtHauteur) && ValiderChamp(this.txtLargeur) && ValiderPictureBox())
            {
                List<(Color, double)> lstQttyCouleur = new List<(Color, double)>();
                foreach (ObjetDessin objetDessin in this._pileObjetDessins)
                {
                    if (objetDessin is Forme forme)
                    {
                        AdditionnerAireCouleur(forme, lstQttyCouleur);
                    }
                }

                this.lbResultatsCalculPeinture.Items.Clear();
                AfficherResultatCalcul(lstQttyCouleur);
            }
        }

        #endregion Événements

        #region Méthodes

        /// <summary>
        /// Méthode permettant d'ajouter une ligne au PictureBox
        /// </summary>
        /// <param name="pPositionSouris">Point représentant la position du pointeur de la souris sur le canvas de dessin</param>
        private void DessinerLigne(MouseEventArgs pPositionSouris)
        {
            _pileObjetDessins.Push(new Ligne(couleurBordure.Color, _pntPositionDepart,
                new Point(pPositionSouris.X, pPositionSouris.Y), int.Parse(cmbTrait.Text)));
        }

        /// <summary>
        /// Méthode permettant d'ajouter un rectangle au PictureBox.
        /// </summary>
        /// <param name="pPointDepart">Point représentant le départ du dessin</param>
        /// <param name="pPointFin">Point représentant la fin du dessin</param>
        private void DessinerRectangle(Point pPointDepart, Point pPointFin)
        {
            _pileObjetDessins.Push(new Rectangle(couleurBordure.Color, couleurRemplissage.Color,
                pPointDepart, pPointFin, int.Parse(cmbTrait.Text)));
        }

        /// <summary>
        /// Méthode permettant d'ajouter une Ellipse au PictureBox
        /// </summary>
        /// <param name="pPointDepart">Point représentant le départ du dessin</param>
        /// <param name="pPointFin">Point représentant la fin du dessin</param>
        private void DessinerEllipse(Point pPointDepart, Point pPointFin)
        {
            _pileObjetDessins.Push(new Ellipse(couleurBordure.Color, this.couleurRemplissage.Color, pPointDepart,
                pPointFin, int.Parse(cmbTrait.Text)));
        }

        /// <summary>
        /// Méthode permettant de calculer la taille réel en mètre de la
        /// longeur de la forme à l'intérieur du canvas de dessin
        /// </summary>
        /// <param name="pLongeurDemande">Longeur réel en mètre d'un des cotés
        /// demandé par l'utilisateur de la surface de dessin</param>
        /// <param name="pLongeurForme">Longeur d'un coté de la forme (hauteur ou largeur)</param>
        /// <param name="pLongueurCanvas">Longeur du coté du canvas de dessin correspondant</param>
        /// <returns>Taille réel en mètre de la longueur de la forme</returns>
        private static double CalculerTailleReel(double pLongeurDemande, double pLongeurForme, float pLongueurCanvas)
        {
            return pLongeurForme * pLongeurDemande / pLongueurCanvas;
        }

        /// <summary>
        /// Méthode permettant l'affichage des resultats du calcul
        /// </summary>
        /// <param name="pListeQuantiteCouleur">Liste de Tuple représentant chaque couleur et leur quantité nécessaire</param>
        private void AfficherResultatCalcul(List<(Color, double)> pListeQuantiteCouleur)
        {
            foreach ((Color, double) tupleCouleurQtty in pListeQuantiteCouleur)
            {
                string resultat = string.Format("Couleur : {0} Quantité : {1}", tupleCouleurQtty.Item1.Name,
                    tupleCouleurQtty.Item2);
                this.lbResultatsCalculPeinture.Items.Add(resultat);
            }
        }

        /// <summary>
        /// Méthode permettant la validation d'un champ texteBox pour qu'il ne contienne que des chiffres
        /// </summary>
        /// <param name="pChampAValider">TextBoxt à valider</param>
        /// <returns>Retourne Vrai si le champ est valide, sinon Faux</returns>
        private bool ValiderChamp(TextBox pChampAValider)
        {
            bool estValide = true;
            if (pChampAValider.Text.Length > 0)
            {
                int i = 0;
                while (i < pChampAValider.Text.Length && estValide)
                {
                    if (pChampAValider.Text[i] < '0' || pChampAValider.Text[i] > '9')
                    {
                        estValide = false;
                        this.errorProvider1.SetError(pChampAValider, "Ce champ doit contenir que des chiffres");
                    }

                    i++;
                }
            }
            else
            {
                estValide = false;
                this.errorProvider1.SetError(pChampAValider, "Ce champ est obligatoire");
            }

            return estValide;
        }

        /// <summary>
        /// Méthode permettant la validation du PictureBox pour s'assurer qu'il ne soit pas vide
        /// </summary>
        /// <returns>Retourne Vrai, si le champ est valide sinon Faux</returns>
        private bool ValiderPictureBox()
        {
            bool estValide = true;
            if (this._pileObjetDessins.Count <= 0)
            {
                errorProvider1.SetError(this.pbDessin, "Vous devez avoir des dessins avant de pouvoir faire un calcul");
                estValide = false;
            }

            return estValide;
        }

        /// <summary>
        /// Méthode permettant d'ajouter un Tuple Couleur/Quantité
        /// dans la liste envoyé en paramètre
        /// </summary>
        /// <param name="pForme">La forme</param>
        /// <param name="pLstQttyCouleur">La liste de Tuple composé d'une couleur et d'une aire</param>
        private void AdditionnerAireCouleur(Forme pForme, List<(Color, double)> pLstQttyCouleur)
        {
            double largeurDemande = Convert.ToDouble(txtLargeur.Text);
            double hauteurDemande = Convert.ToDouble(txtHauteur.Text);
            Color couleurForme = pForme.CouleurRemplissage;
            double largeurForme = pForme.PointFin.X - pForme.PointDepart.X;
            double hauteurForme = pForme.PointFin.Y - pForme.PointDepart.Y;
            double largeurReel = frmAccueil.CalculerTailleReel(largeurDemande, largeurForme, frmAccueil.largeurCanvas);
            double hauteurReel = frmAccueil.CalculerTailleReel(hauteurDemande, hauteurForme, frmAccueil.hauteurCanvas);
            double aireForme = pForme.CalculerAire(hauteurReel, largeurReel);
            if (pLstQttyCouleur.Count == 0)
            {
                pLstQttyCouleur.Add((couleurForme, aireForme));
            }
            else
            {
                if (frmAccueil.ContientCouleur(pLstQttyCouleur, couleurForme).Item1)
                {
                    int index = frmAccueil.ContientCouleur(pLstQttyCouleur, couleurForme).Item2;
                    pLstQttyCouleur.Add((couleurForme, pLstQttyCouleur[index].Item2 + aireForme));
                    pLstQttyCouleur.Remove(pLstQttyCouleur[index]);
                }
                else
                {
                    pLstQttyCouleur.Add((couleurForme, aireForme));
                }
            }
        }

        /// <summary>
        /// Méthode permettant de savoir si la liste de Tuple envoyé en paramètre
        /// contient la couleur envoyer en paramètre.
        /// </summary>
        /// <param name="pLstQttyCouleur">Liste de Tuple composé de Couleur et de sa quantité</param>
        /// <param name="pCouleurForme">La couleur recherché</param>
        /// <returns>Retourne Vrai si la couleur est déja présente dans la liste, sinon Faux.</returns>
        private static Tuple<bool, int> ContientCouleur(List<(Color, double)> pLstQttyCouleur, Color pCouleurForme)
        {
            int index = 0;
            foreach ((Color, double) tupleQuantiteCouleur in pLstQttyCouleur)
            {
                if (tupleQuantiteCouleur.Item1 == pCouleurForme)
                {
                    return new Tuple<bool, int>(true, index);
                }

                index++;
            }

            return new Tuple<bool, int>(false, index);
        }

        #endregion Méthodes
    }
}